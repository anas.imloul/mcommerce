const Product = require('../model/product');

class ProductDao {

    static findAll() {
        return Product.find().exec();
    }

    static findById(orderId) {
        return Product.findOne({ orderId }).exec();
    }

    static save(payment) {
        const newPayment = new Payment(payment);
        return newPayment.save();
    }
}

module.exports = ProductDao;
