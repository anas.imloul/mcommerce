const ProductDao = require('../dao/product-dao');
const ProductNotFoundException = require('../exceptions/product-not-found-exception');

const getProducts = async (req, res) => {
    try {
        const products = await ProductDao.findAll();

        if (products.length === 0) {
            throw new ProductNotFoundException("No products are available for sale");
        }

        res.json(products);
    } catch (error) {
        res.status(404).json({ error: error.message });
    }
}

const createProducts = async (req, res) => {
    const product = req.body;

    try {
        const createdProduct = await ProductDao.create(product);

        res.status(201).json(createdProduct);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }

}

const getProductById = async (req, res) => {
    const productId = req.params.id;

    try {
        const product = await ProductDao.findById(productId);

        if (!product) {
            throw new ProductNotFoundException(`The product with id ${productId} does not exist`);
        }

        res.json(product);
    } catch (error) {
        res.status(404).json({ error: error.message });
    }
}

module.exports = {
    getProducts,
    getProductById
}
