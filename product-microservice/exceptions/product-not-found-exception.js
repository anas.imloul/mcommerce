class ProductNotFoundException extends Error {
    constructor(message) {
        super(message);
        this.name = 'ProductNotFoundException';
    }
}

module.exports = ProductNotFoundException;