const express = require('express');
const router= express.Router();

const { getProducts, getProductById, createProducts } = require('../controllers/product-controller');

router.get('', getProducts);
router.get('/:id', getProductById);
router.post('', createProducts);


module.exports = router;
