const express = require('express');
const { createProxyMiddleware } = require('http-proxy-middleware');
const morgan = require('morgan');
const cors = require('cors');
const dotenv = require('dotenv');

dotenv.config();

const app = express();

app.use(
    cors({
      origin: ['*'],
    })
);

const ports = {
  '/products': 3001,
  '/orders': 3002,
  '/payment': 3003,
};

app.use(morgan('combined'));

for (const [service, port] of Object.entries(ports)) {
  app.use(service, createProxyMiddleware({ target: `http://localhost:${port}`, changeOrigin: true }));
}

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Mcommerce API is running on port ${PORT}`);
});
