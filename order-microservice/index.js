const express = require('express');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const orderRoutes = require('./routes/order-routes');
const mongoose = require('mongoose');


const app = express();
dotenv.config();

mongoose.connect(process.env.MONGODB_URI);

const db = mongoose.connection;


db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', () => {
    console.log('Connected to MongoDB');
});

app.use(bodyParser.json());

app.use('/orders', orderRoutes);

const PORT = process.env.PORT;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
