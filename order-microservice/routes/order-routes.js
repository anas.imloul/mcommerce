const express = require('express');
const router = express.Router();

const {
    updateOrder,
    getOrderById,
    createOrder,
} = require('../controllers/order-controller');


router.post('/orders', createOrder);
router.get('/orders/:id', getOrderById);
router.put('/orders/:id', updateOrder);

module.exports = router;
