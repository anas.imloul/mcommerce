const Order = require("../models/order");

class OrderDao {
    findById(id) {
        return Order.findById(id).exec();
    }
    create(order) {
        return Order.create(order);
    }
    update(id, order) {
        return Order.findByIdAndUpdate(id, order, { new: true }).exec();
    }
}

module.exports = OrderDao;
