const express = require('express');
const OrderDao = require('../dao/order-dao');


const orderDao = new OrderDao();

const updateOrder = async (req, res) => {
    try {
        const order = await orderDao.update(req.params.id, req.body);

        if (!order) {
            res.status(404).send('Order not found');
            return;
        }

        res.status(200).json(order);
    } catch (error) {
        console.error(error.message);
        res.status(500).send('Internal Server Error');
    }
};

const getOrderById = async (req, res) => {
    try {
        const order = await orderDao.findById(req.params.id);

        if (!order) {
            res.status(404).send('Order not found');
            return;
        }

        res.status(200).json(order);
    } catch (error) {
        console.error(error.message);
        res.status(500).send('Internal Server Error');
    }
};

const createOrder = async (req, res) => {
    try {
        const order = await orderDao.create(req.body);

        res.status(201).json(order);
    } catch (error) {
        console.error(error.message);
        res.status(500).send('Internal Server Error');
    }
};


module.exports = {updateOrder, getOrderById, createOrder};
