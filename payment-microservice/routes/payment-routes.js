const express = require('express');
const processPayment = require('../controllers/payment-controller');

const paymentRouter = express.Router();

paymentRouter.post('', processPayment);

module.exports = paymentRouter;
