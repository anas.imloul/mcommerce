const Payment = require('../model/payment');

class PaymentDao {

    static findAll() {
        return Payment.find().exec();
    }

    static findByOrderId(orderId) {
        return Payment.findOne({ orderId }).exec();
    }

    static save(payment) {
        const newPayment = new Payment(payment);
        return newPayment.save();
    }
}

module.exports = PaymentDao;
