const PaymentDao = require('../dao/payment-dao');
const MicroserviceCommandProxy = require('../proxies/order-service-proxy');
const microserviceCommandProxy = new MicroserviceCommandProxy(process.env.ORDER_SERVICE_URL);

const processPayment = async (req, res) => {
  const payment = req.body;

  try {
    console.log('Processing payment:', payment)
    console.log(await PaymentDao.findAll())

    if (!payment.orderId || !payment.email) {
        throw new Error('Order information missing');
    }

    // Check if a payment already exists for this order
    const existingPayment = await PaymentDao.findByOrderId(payment.orderId);
    if (existingPayment) {
      throw new Error('This order has already been paid');
    }

    // Save the payment
    const newPayment = await PaymentDao.save(payment);

    if (!newPayment) {
      throw new Error('Error, unable to process the payment, please try again later');
    }

    // Retrieve the corresponding order using the Microservice Commands
    const order = await microserviceCommandProxy.retrieveOrder(payment.orderId);

    // Update the status of the order in the Microservice Commands
    await microserviceCommandProxy.updateOrder({
      ...order, orderPaid: true,
    });

    // Respond with 201 CREATED to notify the client that the payment has been recorded
    res.status(201).json(newPayment);
  } catch (error) {
    // Handle errors
    console.error('Error processing payment:', error.message);
    res.status(500).json({ error: error.message });
  }
};

module.exports = processPayment;
