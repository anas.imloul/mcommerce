const axios = require('axios');


class OrderServiceProxy {
    constructor(baseUrl) {
        this.baseUrl = baseUrl;
    }

    async retrieveOrder(id) {
        try {
            const response = await axios.get(`${this.baseUrl}/order/${id}`);
            return response.data;
        } catch (error) {
            // Handle error
            console.error(`Error retrieving order with id ${id}:`, error.message);
            throw error;
        }
    }

    async updateOrder(order) {
        try {
            await axios.put(`${this.baseUrl}/order`, order);
        } catch (error) {
            // Handle error
            console.error('Error updating order:', error.message);
            throw error;
        }
    }
}

module.exports = OrderServiceProxy;
