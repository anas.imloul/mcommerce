const mongoose = require("mongoose");

const PaymentSchema = new mongoose.Schema({
  orderId: String,
  email: String,
});
const Payment = mongoose.model("Payment", PaymentSchema);
module.exports = Payment;
