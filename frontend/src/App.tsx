// App.js
import React from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import ProductListPage from './components/products-page';

const App = () => {
  return (
      <Router>
          <Routes>
              <Route path="/" Component={ProductListPage} />
          </Routes>
      </Router>
  );
};

export default App;
