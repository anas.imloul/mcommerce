
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Product } from '../../types';
import { ProductComponent } from '../product';

const ProductListPage = () => {
    const [products, setProducts] = useState<Array<Product>>([]);

    useEffect(() => {
        // Fetch products from the backend API
        const fetchProducts = async () => {
            try {
                const response = await axios.get('http://localhost:3001/products');
                setProducts(response.data);
            } catch (error) {
                console.error('Error fetching products:', error);
            }
        };

        fetchProducts();
    }, []);

    return (
        <div>
            <h1>All Products</h1>
            <div className="product-grid">
                {products.map((product) => (
                    <ProductComponent
                        name={product.name}
                        description={product.description}
                        image={product.image}
                        price={product.price} />
                ))}
            </div>
        </div>
    );
};

export default ProductListPage;
