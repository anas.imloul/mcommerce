import {useState, useId, useCallback} from "react";
import { Product } from "../../types";


export const ProductComponent = (product: Product) => {
    const id = useId();

    const OnViewDetailsClicked = useCallback(() => {
        console.log("View Details Clicked");
    }, [product]);

    return (
        <div key={id} className="product-card">
            <img src={product.image} alt={product.name} />
            <h3>{product.name}</h3>
            <p>{product.description}</p>
            <p>Price: ${product.price}</p>
            <button onClick={OnViewDetailsClicked}>View Details</button>
        </div>
    )
}